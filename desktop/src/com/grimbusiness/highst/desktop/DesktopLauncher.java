package com.grimbusiness.highst.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.grimbusiness.highst.highst;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 720/2;
        config.height = 1280/2;
        config.resizable = false;
		new LwjglApplication(new highst(), config);
	}
}
