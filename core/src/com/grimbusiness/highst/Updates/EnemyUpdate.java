package com.grimbusiness.highst.Updates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.grimbusiness.highst.Calculations;
import com.grimbusiness.highst.Collision;
import com.grimbusiness.highst.WorldObjects.Enemy;
import com.grimbusiness.highst.highst;

import java.util.Iterator;

/**
 * Created by Rónán on 25/06/2014.
 */
public class EnemyUpdate extends highst {

    public static void updateEnemies(){
        if(timeToEnemy > Calculations.calculateFall(floor)){
            timeToEnemy = 0;
            Enemy enemy = new Enemy((int)screenHeight, (int)screenWidth, enemyTextures.get(rng.nextInt(enemyTextures.size())));
            Sprite eSprite = new Sprite(enemy.getTexture());
            eSprite.setOrigin(0, 0);
            eSprite.rotate(rng.nextFloat() * 360);
            enemies.add(enemy);
            enemySprites.add(eSprite);
        } else{
            timeToEnemy += Gdx.graphics.getDeltaTime();
            if(rng.nextInt(100) < 20)
                timeToEnemy += 0.1f;
        }

        enemyCollisions();
    }

    private static void enemyCollisions() {
        int counter = 0;
        for (Iterator<Enemy> i = enemies.iterator(); i.hasNext();) {
            Enemy item = i.next();
            if (item.getPosY() < -100) {
                i.remove();
                enemySprites.remove(counter);
            } else if (Collision.playerCollidesWithWorldObject(item, item.getTexture(), playerTexture, player, 0)) {
                if(player.getDrunkLevel() == 4)
                    gameState = states.dead;
                else
                    player.setDrunkLevel(player.getDrunkLevel()+1);
                i.remove();
                enemySprites.remove(counter);
            } else {
                item.setVelY(-3.0f);
                item.setPosY(item.getVelY() + item.getPosY());
                enemySprites.get(counter).setPosition(item.getPosX(),
                        item.getPosY());
                counter++;
            }
        }
    }

}
