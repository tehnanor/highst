package com.grimbusiness.highst.Updates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.grimbusiness.highst.Calculations;
import com.grimbusiness.highst.Collision;
import com.grimbusiness.highst.WorldObjects.Diamond;
import com.grimbusiness.highst.highst;

import java.util.Iterator;

/**
 * Created by Rónán on 25/06/2014.
 */
public class DiamondUpdate extends highst {

    public static void updateDiamonds() {
        if (timeToDiamond > Calculations.calculateFall(floor)) {
            timeToDiamond = 0;
            Diamond diamond = new Diamond((int) screenHeight, (int) screenWidth);
            Sprite dSprite = new Sprite(diamondTexture);
            dSprite.setOrigin(0, 0);
            diamonds.add(diamond);
            diamondSprites.add(dSprite);
        } else {
            timeToDiamond += Gdx.graphics.getDeltaTime();
            if(rng.nextInt(100) < 20)
                timeToDiamond += 0.1f;
        }

        diamondCollisions();
    }

    protected static void diamondCollisions() {

        int counter = 0;
        for (Iterator<Diamond> i = diamonds.iterator(); i.hasNext();) {
            Diamond item = i.next();
            if (item.getPosY() < -100) {
                i.remove();
                diamondSprites.remove(counter);
            } else if (Collision.playerCollidesWithWorldObject(item, diamondTexture, playerTexture, player, 20)) {
                score++;
                i.remove();
                diamondSprites.remove(counter);
            } else {
                item.setVelY(-3.0f);
                item.setPosY(item.getVelY() + item.getPosY());
                diamondSprites.get(counter).setPosition(item.getPosX(),
                        item.getPosY());
                counter++;
            }
        }

    }

}
