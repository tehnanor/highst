package com.grimbusiness.highst.Updates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.grimbusiness.highst.Collision;
import com.grimbusiness.highst.WorldObjects.Elevator;
import com.grimbusiness.highst.highst;

import java.util.Iterator;
import java.util.Random;

public class ElevatorUpdate extends highst {

    float elevatorSpawnTime;
    float timeToElevator;

    public ElevatorUpdate(){
        elevatorSpawnTime = 5 + new Random().nextFloat() + new Random().nextInt(5);
        timeToElevator = 0;
    }

    public void updateElevators() {
        if (timeToElevator > 5) {
            timeToElevator = 0;
            Elevator elevator = new Elevator((int) screenHeight, (int) generateElevatorXPos());
            Sprite eSprite = new Sprite(elevatorTexture);
            eSprite.setOrigin(0, 0);
            elevators.add(elevator);
            elevatorSprites.add(eSprite);
        } else {
            timeToElevator += Gdx.graphics.getDeltaTime();
        }

        calculateElevatorMovement();
        elevatorCollisions();
    }


    public void calculateElevatorMovement() {
        int counter = 0;
        for (Iterator<Elevator> i = elevators.iterator(); i.hasNext(); ) {
            Elevator item = i.next();

            //chance to randomly set the elevator to stopping
            if(item.getTimeSinceLastStop() > 10.0f){
                if(!item.isStopping()) {
                    if (rng.nextInt(100) == 10) {
                        item.setStopping(true);
                        item.setTimeToStop(rng.nextInt(5) + 5);
                    }
                }
            } else {
                item.setTimeSinceLastStop(item.getTimeSinceLastStop() + Gdx.graphics.getDeltaTime());
            }

            if (item.getPosY() < -100) {
                i.remove();
                elevatorSprites.remove(counter);
            } else if (Collision.playerCollidesWithWorldObject(item, elevatorTexture, playerTexture, player, 10)) {
                gameState = states.dead;
                i.remove();
                elevatorSprites.remove(counter);
            } else {
                if(!item.isStopping())
                    item.setVelY(item.isAscending() ? -3 : 3);
                else if(item.isWaiting()){
                    item.setVelY(0);
                    if(item.getTimeWaiting() >= item.getTimeToStop()) {
                        item.setWaiting(false);
                        item.setStopping(false);
                    } else {
                        item.setTimeWaiting(item.getTimeWaiting() + Gdx.graphics.getDeltaTime());
                    }

                }
                else {
                    if(item.getVelY() >= -3.0f && item.getVelY() <= 3.0f)
                        item.setVelY(item.isAscending() ? item.getVelY() + 0.1f : item.getVelY() - 0.1f);

                    if(item.getVelY() <=0.15f && item.getVelY() >= -0.15f) {
                        item.setAscending(!item.isAscending());
                        item.setWaiting(true);
                    }


                }
                item.setPosY(item.getVelY() + item.getPosY());
                elevatorSprites.get(counter).setPosition(item.getPosX(),
                        item.getPosY());
                counter++;
            }

            if(item.getTimeIgnoreCollisions() > 3){
                item.setIgnoreCollisions(false);
            } else{
                item.setTimeIgnoreCollisions(item.getTimeIgnoreCollisions() + Gdx.graphics.getDeltaTime());
            }

        }
    }

    private void elevatorCollisions(){
        for(int outer = 0; outer < elevators.size(); outer++){
            for(int inner = 0 ; inner < elevators.size(); inner++){
                if(outer != inner){
                    if(Collision.worldObjectCollidesWithWorldObject(elevators.get(outer), elevatorTexture, elevators.get(inner), elevatorTexture, 100)){
                        //elevators.get(outer).setAscending(!elevators.get(outer).isAscending());
                        //elevators.get(inner).setAscending(!elevators.get(inner).isAscending());
                        if((!elevators.get(outer).isStopping() || !elevators.get(outer).isWaiting()) && !elevators.get(outer).isIgnoreCollisions()){
                            System.out.println(outer + "stopped");
                            elevators.get(outer).setStopping(true);
                            elevators.get(outer).setIgnoreCollisions(true);
                        }
                        if((!elevators.get(inner).isStopping() || !elevators.get(inner).isWaiting()) && !elevators.get(inner).isIgnoreCollisions()){
                            System.out.println(inner + "stopped");
                            elevators.get(inner).setStopping(true);
                            elevators.get(inner).setIgnoreCollisions(true);
                        }
                    }
                }
            }
        }
    }

    static float generateElevatorXPos() {
        if (rng.nextInt(2) == 0)
            return 0;
        else
            return screenWidth - elevatorTexture.getWidth();
    }
}