package com.grimbusiness.highst.WorldObjects;

import com.badlogic.gdx.graphics.Texture;
import com.grimbusiness.highst.WorldObjects.WorldObject;

import java.util.Random;

/**
 * Created by Rónán on 12/06/2014.
 */
public class Enemy extends WorldObject {

    private static Random rng = new Random();

    private Texture texture;

    public Enemy(int screenHeight, int screenWidth, Texture texture) {
        super(40 + rng.nextInt(screenWidth-80), screenHeight, 0, 0);
        this.texture = texture;
    }

    public Texture getTexture(){
        return texture;
    }
}
