package com.grimbusiness.highst.WorldObjects;

import java.util.Random;

public class Elevator extends WorldObject {

    private boolean waiting;
    private boolean ascending;
    private boolean stopping;
    private boolean ignoreCollisions;
    private float timeToStop;
    private float timeSinceLastStop;
    private float timeWaiting;
    private float timeIgnoreCollisions;

	public Elevator(int screenHeight, int xPos) {
        super(xPos, screenHeight+500, 0, 0);
        ascending = new Random().nextBoolean();
        stopping = false;
        waiting = false;
        timeWaiting = 0;
	}

    public boolean isAscending(){
        return ascending;
    }

    public void setAscending(boolean ascending){
        this.ascending = ascending;
    }

    public boolean isStopping(){
        return stopping;
    }

    public void setStopping(boolean stopping){
        this.stopping = stopping;
    }

    public float getTimeSinceLastStop() {
        return timeSinceLastStop;
    }

    public void setTimeSinceLastStop(float timeSinceLastStop) {
        this.timeSinceLastStop = timeSinceLastStop;
    }

    public float getTimeToStop() {
        return timeToStop;
    }

    public void setTimeToStop(float timeToStop) {
        this.timeToStop = timeToStop;
    }

    public boolean isWaiting() {
        return waiting;
    }

    public void setWaiting(boolean waiting) {
        this.waiting = waiting;
    }

    public float getTimeWaiting() {
        return timeWaiting;
    }

    public void setTimeWaiting(float timeWaiting) {
        this.timeWaiting = timeWaiting;
    }

    public boolean isIgnoreCollisions() {
        return ignoreCollisions;
    }

    public void setIgnoreCollisions(boolean ignoreCollisions) {
        this.ignoreCollisions = ignoreCollisions;
    }

    public float getTimeIgnoreCollisions() {
        return timeIgnoreCollisions;
    }

    public void setTimeIgnoreCollisions(float timeIgnoreCollisions) {
        this.timeIgnoreCollisions = timeIgnoreCollisions;
    }
}
