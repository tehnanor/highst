package com.grimbusiness.highst.WorldObjects;

public class WorldObject {

	protected float posX;
	protected float posY;
	protected float velX, velY;
	
	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}

	public float getVelX() {
		return velX;
	}

	public void setVelX(float velX) {
		this.velX = velX;
	}

	public float getVelY() {
		return velY;
	}

	public void setVelY(float velY) {

        this.velY = velY;


	}

	public WorldObject(float posX, float posY, float velX, float velY) {
		this.posX = posX;
		this.posY = posY;
		this.velX = velX;
		this.velY = velY;
	}
	
}
