package com.grimbusiness.highst.WorldObjects;

public class Player extends WorldObject {

	private boolean isFacingLeft;
    private int drunkLevel;

	
	public boolean canJump(float screenWidth, int playerWidth) {
		if(posX <= playerWidth || posX >= screenWidth - playerWidth*2 || posY < 0)
			return true;
		else
			return false;
	}

	public Player(int posX, int posY) {
		super(posX, posY, 100, 0);
		this.isFacingLeft = true;
        drunkLevel = 0;
	}

	public boolean isFacingLeft() {
		return isFacingLeft;
	}

	public void setFacingLeft(boolean isFacingLeft) {
		this.isFacingLeft = isFacingLeft;
	}

    public int getDrunkLevel(){return drunkLevel;}

    public void setDrunkLevel(int drunkLevel){this.drunkLevel = drunkLevel;}


    public void setStart() {
        posX = 0;
        posY = 100;
        velY = 0;
        velX = 0;
    }
}
