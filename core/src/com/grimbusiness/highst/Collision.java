package com.grimbusiness.highst;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.grimbusiness.highst.WorldObjects.*;

import java.util.ArrayList;

public class Collision {

    public static boolean playerCollidesWithWorldObject(WorldObject obj, Texture objTexture, Texture playerTexture, Player player, int offset){
        Rectangle object = new Rectangle(obj.getPosX() - offset, obj.getPosY() - offset,
                objTexture.getWidth() + offset, objTexture.getHeight() + offset);
        Rectangle playerRect = new Rectangle(player.getPosX(),
                player.getPosY(), playerTexture.getWidth(),
                playerTexture.getHeight());
        return Intersector.overlaps(object, playerRect);
    }

    public static boolean worldObjectCollidesWithWorldObject(WorldObject objOne, Texture objOneTexture, WorldObject objTwo, Texture objTwoTexture, int offset){
        Rectangle objectOne = new Rectangle(objOne.getPosX() + offset, objOne.getPosY() + offset,
                objOneTexture.getWidth() + offset, objOneTexture.getHeight() + offset);
        Rectangle objectTwo = new Rectangle(objTwo.getPosX() + offset, objTwo.getPosY() +offset,
                objTwoTexture.getWidth() + offset, objTwoTexture.getHeight() + offset);

        return Intersector.overlaps(objectOne, objectTwo);
    }

	protected static String wallCollision(Player player, int playerWidth, float screenWidth, float timeToJump) {
		if (player.getPosX() < playerWidth)
			return "left";
		else if (player.getPosX() > screenWidth - playerWidth * 2)
			return "right";
		else {
			timeToJump += Gdx.graphics.getDeltaTime();
			if (timeToJump > 0.50f) {
				timeToJump = 0;
			}
			return "jumping";
		}
	}

}
