package com.grimbusiness.highst;

import java.util.Random;

/**
 * Created by Rónán on 11/06/2014.
 */
public class Calculations {

    static public float calculateFall(int floor){

        float speed = (floor / 10) - 7.0f;

        if(-speed < 0.5f){
            return 0.5f;
        }
        else{
            return -speed;
        }
    }

    static public int calculateDrunkRotation(int drunkLevel){
        if(new Random().nextBoolean()){
            return drunkLevel;
        } else{
            return -drunkLevel;
        }
    }


}
