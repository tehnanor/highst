package com.grimbusiness.highst;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.grimbusiness.highst.Updates.DiamondUpdate;
import com.grimbusiness.highst.Updates.ElevatorUpdate;
import com.grimbusiness.highst.Updates.EnemyUpdate;
import com.grimbusiness.highst.WorldObjects.Diamond;
import com.grimbusiness.highst.WorldObjects.Elevator;
import com.grimbusiness.highst.WorldObjects.Enemy;
import com.grimbusiness.highst.WorldObjects.Player;

import java.lang.Override;
import java.lang.String;
import java.util.*;

public class highst implements ApplicationListener, InputProcessor {

    protected static Texture playerTexture;
    protected static Texture diamondTexture;
    protected static Player player;
    protected static float timeToDiamond = 0;
    protected static float timeToEnemy = 0;
    protected static float screenHeight;
    protected static float screenWidth;
    protected static ArrayList<Diamond> diamonds = new ArrayList<Diamond>();
    protected static ArrayList<Sprite> diamondSprites = new ArrayList<Sprite>();
    protected static ArrayList<Sprite> enemySprites = new ArrayList<Sprite>();
    protected static ArrayList<Enemy> enemies = new ArrayList<Enemy>();
    protected static ArrayList<Texture> enemyTextures = new ArrayList<Texture>();
    protected static int score;
    protected static Random rng = new Random();
    protected static int floor;
    protected static states gameState = states.menu;
    static SpriteBatch spriteBatch;
    protected static Texture elevatorTexture;
    static Texture backgroundTexture;
    static Texture wineTexture;
    static Texture beerTexture;
    static Texture cableTexture;
    static TextureAtlas drunkAtlas;

    static TextureRegion drunkBar0;
    static TextureRegion drunkBar1;
    static TextureRegion drunkBar2;
    static TextureRegion drunkBar3;
    static TextureRegion drunkBar4;
    static TextureRegion[] drunkBars = new TextureRegion[5];

    static TextureRegion jumpBar0, jumpBar1, jumpBar2, jumpBar3, jumpBar4;
    static TextureRegion[] jumpBars = new TextureRegion[5];

    ElevatorUpdate elevatorUpdate;

    static FreeTypeFontGenerator fontGenerator;
    static BitmapFont fontScaled;
    static Sprite playerSprite;
    static Viewport viewport;
    static OrthographicCamera camera;
    static float gravity = -0.3f;
    static float timeToJump = 0;
    static float timeTouched;
    static float[] backgroundPositions = new float[3];
    static int[] backgroundIndexes = new int[3];
    protected static ArrayList<Elevator> elevators = new ArrayList<Elevator>();
    protected static ArrayList<Sprite> elevatorSprites = new ArrayList<Sprite>();
    static boolean started = false, collisionLeft = false,
            collisionRight = false, touchDown = false;
    static int playerWidth;
    static int playerHeight;
    static int scale = 1;
    static int bgIndex = 0;
    static int height;
    static Texture playerJumpTexture;
    static String startText =
            "    How to play\n" +
                    "Hold to charge jump\n\n" +
                    "Avoid elevators\n" +
                    "and avoid alcohol\n\n" +
                    "\n Alpha 0.2";
    ArrayList<Texture> backgroundTextures = new ArrayList<Texture>();

    @Override
    public void create() {
        screenWidth = 720;
        screenHeight = 1280;

        camera = new OrthographicCamera();
        camera.position.set(100, 100, 0);
        camera.update();

        viewport = new StretchViewport(screenWidth, screenHeight, camera);

        fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("data/font2.ttf"));
        fontScaled = fontGenerator.generateFont(32);

        fontScaled.setColor(Color.BLACK);

        spriteBatch = new SpriteBatch();

        playerTexture = new Texture(Gdx.files.internal("data/player.png"));
        playerTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        playerJumpTexture = new Texture(Gdx.files.internal("data/playerJumping.png"));
        playerJumpTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        diamondTexture = new Texture(Gdx.files.internal("data/ruby.png"));
        diamondTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        elevatorTexture = new Texture(Gdx.files.internal("data/elevator.png"));
        elevatorTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        wineTexture = new Texture(Gdx.files.internal("data/wineBottle.png"));
        wineTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        enemyTextures.add(wineTexture);

        beerTexture = new Texture(Gdx.files.internal("data/beerBottle.png"));
        beerTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        enemyTextures.add(beerTexture);

        cableTexture = new Texture(Gdx.files.internal("data/cables.png"));
        cableTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);


        for (int i = 1; i <= 3; i++) {
            backgroundTexture = new Texture(
                    Gdx.files.internal("data/background" + i + ".png"));
            backgroundTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
            backgroundTextures.add(backgroundTexture);
        }

        drunkAtlas = new TextureAtlas(Gdx.files.internal("data/drunkBar.pack"));
        drunkBar0 = drunkAtlas.findRegion("drunk0");
        drunkBar1 = drunkAtlas.findRegion("drunk1");
        drunkBar2 = drunkAtlas.findRegion("drunk2");
        drunkBar3 = drunkAtlas.findRegion("drunk3");
        drunkBar4 = drunkAtlas.findRegion("drunk4");

        jumpBar0 = drunkAtlas.findRegion("drunk0");
        jumpBar1 = drunkAtlas.findRegion("drunk1");
        jumpBar2 = drunkAtlas.findRegion("drunk2");
        jumpBar3 = drunkAtlas.findRegion("drunk3");
        jumpBar4 = drunkAtlas.findRegion("drunk4");

        drunkBars[0] = drunkBar0;
        drunkBars[1] = drunkBar1;
        drunkBars[2] = drunkBar2;
        drunkBars[3] = drunkBar3;
        drunkBars[4] = drunkBar4;

        jumpBars[0] = jumpBar0;
        jumpBars[1] = jumpBar1;
        jumpBars[2] = jumpBar2;
        jumpBars[3] = jumpBar3;
        jumpBars[4] = jumpBar4;

        reset();

        playerWidth = (int) (playerSprite.getWidth() * scale);
        playerHeight = (int) (playerSprite.getHeight() * scale);

        Gdx.input.setInputProcessor(this);

    }

    private void reset() {
        backgroundIndexes[0] = 0;
        backgroundIndexes[1] = 1;
        backgroundIndexes[2] = 2;

        backgroundPositions[0] = 0;
        backgroundPositions[1] = screenHeight;
        backgroundPositions[2] = screenHeight * 2;

        enemies.clear();
        enemySprites.clear();

        diamonds.clear();
        diamondSprites.clear();

        elevators.clear();
        elevatorSprites.clear();

        score = 0;
        floor = 0;

        collisionLeft = false;
        collisionRight = false;

        player = new Player(Gdx.graphics.getWidth() / 2 - playerTexture.getWidth(), 0);

        height = (int) screenHeight - 700 + (int) player.getPosY();

        playerSprite = new Sprite(playerTexture);
        playerSprite.setOrigin(0, 0);
        playerSprite.setPosition(player.getPosX(), player.getPosY());
        playerSprite.scale(scale);

        elevatorUpdate = new ElevatorUpdate();
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.setProjectionMatrix(camera.projection);
        spriteBatch.setTransformMatrix(camera.view);
        spriteBatch.begin();


        switch (gameState) {
            case menu:
                spriteBatch.draw(backgroundTextures.get(bgIndex), 0, 0);
                fontScaled.drawMultiLine(spriteBatch, startText, 0,
                        screenHeight - 10);
                break;
            case running:
                updateRunning();
                for (int i = 0; i < backgroundIndexes.length; i++) {
                    spriteBatch.draw(backgroundTextures.get(backgroundIndexes[i]), 0, backgroundPositions[i]);
                }

                spriteBatch.draw(cableTexture, 10, 0);
                spriteBatch.draw(cableTexture, screenWidth - cableTexture.getWidth() - 10, 0);
                playerSprite.draw(spriteBatch);
                fontScaled.draw(spriteBatch, "Alc Lvl: ", 365, screenHeight - 10);
                spriteBatch.draw(drunkBars[player.getDrunkLevel()], screenWidth - drunkBar0.getRegionWidth() - 10, screenHeight - drunkBar0.getRegionHeight() - 10);
                fontScaled.draw(spriteBatch, "Jump Pwr: ", 335, screenHeight - fontScaled.getLineHeight() - 10);
                spriteBatch.draw(drunkBars[Math.round(timeTouched*100/25)], screenWidth - drunkBar0.getRegionWidth() - 10, screenHeight - drunkBar0.getRegionHeight() * 2 - 20);
                fontScaled.draw(spriteBatch, "Score: " + score, 0, screenHeight - 10);
                fontScaled.draw(spriteBatch, "Floor: " + floor, 0, screenHeight - fontScaled.getLineHeight() - 10);
                for (Sprite item : diamondSprites) {
                    item.draw(spriteBatch);
                }
                for (Sprite item : elevatorSprites) {
                    item.draw(spriteBatch);
                }
                for (Sprite item : enemySprites) {
                    item.draw(spriteBatch);
                }
                break;
            case dead:
                reset();
                fontScaled.drawMultiLine(spriteBatch,
                        "You have died!\nTap Anywhere to start again",
                        0, screenHeight - 10);
                break;
        }
        spriteBatch.end();

    }

    private void updateRunning() {
        // if on ground
        if (player.getPosY() <= 0 && !started) {
            player.setPosY(0);
            player.setVelY(0);
        } else if (player.getPosY() < -40 && started) {
            gameState = states.dead;
        } else {
            player.setVelY(player.getVelY() + gravity);
        }

        tapOrHold();
        setDirection();
        EnemyUpdate.updateEnemies();
        DiamondUpdate.updateDiamonds();
        elevatorUpdate.updateElevators();
        updateWorldObjectsOnJump();

        if ((player.getPosY() < screenHeight - 700) || (player.getVelY() < 0)) {
            player.setPosY(player.getPosY() + player.getVelY());
        }

        player.setPosX(player.getPosX() + player.getVelX());

        playerSprite.setPosition(player.getPosX(), player.getPosY());
    }

    private void updateWorldObjectsOnJump() {
        if (player.getPosY() >= screenHeight - 700 && player.getVelY() > 0) {
            height += player.getVelY();

            for (int i = 0; i < backgroundPositions.length; i++) {
                backgroundPositions[i] -= player.getVelY();

                if (backgroundPositions[i] < -screenHeight) {
                    float x = backgroundPositions[0];
                    float y = backgroundPositions[1];
                    float z = backgroundPositions[2];
                    float max = Math.max(x, Math.max(y, z));

                    backgroundPositions[i] = max + backgroundTexture.getHeight() - 30;
                    backgroundIndexes[i] = new Integer(String.valueOf(new Random().nextInt(3)));
                }
            }

            if (height >= screenHeight - playerHeight) {
                height = 0;
                floor++;
            }

            for (Elevator item : elevators)
                item.setPosY(item.getPosY() - player.getVelY());

            for (Diamond item : diamonds)
                item.setPosY(item.getPosY() - player.getVelY());

            for (Enemy item : enemies)
                item.setPosY(item.getPosY() - player.getVelY());
        }

    }

    private void setDirection() {
        // If collides with a wall, switch direction and set velocities
        if (Collision.wallCollision(player, playerWidth, screenWidth, timeToJump).equals("left") && !collisionLeft) {
            player.setPosX(0);
            player.setVelX(0);
            player.setFacingLeft(false);
            collisionLeft = true;
            collisionRight = false;
            playerSprite.setTexture(playerTexture);
            playerSprite.setFlip(false, false);
        } else if (Collision.wallCollision(player, playerWidth, screenWidth, timeToJump).equals("right") && !collisionRight) {
            player.setPosX(screenWidth - playerWidth * 2);
            player.setVelX(0);
            player.setFacingLeft(true);
            collisionRight = true;
            collisionLeft = false;
            playerSprite.setTexture(playerTexture);
            playerSprite.setFlip(true, false);
        } else {
            player.setVelY(player.getVelY() + gravity);

        }
    }

    private void tapOrHold() {
        // Whether tap or hold down
        if (touchDown) {
            if (player.canJump(screenWidth, playerWidth) || !started) {
                if(timeTouched < 1.0f)
                    timeTouched += Gdx.graphics.getDeltaTime();
                player.setVelY(-gravity - 1);
                System.out.println("arf");
            }
        } else {
            if (timeTouched > 0) {
                playerSprite.setTexture(playerJumpTexture);
                if (gameState == states.menu)
                    gameState = states.running;
                else if (gameState == states.running) {
                    if (player.canJump(screenWidth, playerWidth) || !started) {
                        player.setVelX((player.isFacingLeft()) ? -36.0f : 36.0f);
                        player.setVelY(37.5f * timeTouched);
                    }
                    started = true;
                } else if (gameState == states.dead)
                    gameState = states.menu;
            }
            timeTouched = 0;
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        touchDown = true;
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        touchDown = false;

        if (gameState == states.dead) {
            gameState = states.menu;
            player.setStart();
            started = false;
        } else if (gameState == states.menu)
            gameState = states.running;

        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        fontGenerator.dispose();
        fontScaled.dispose();
        spriteBatch.dispose();
        playerTexture.dispose();
    }

    protected enum states {
        menu, running, dead
    }

}
